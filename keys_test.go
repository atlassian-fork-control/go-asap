package asap

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"sync"
	"testing"
	"time"

	"github.com/vincent-petithory/dataurl"
)

const publicKey = `-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzIXNCB3YktXiCiXiN1yR
W+Ox9IqN2aenMKG9NHdOBlwp/2BQkm+G4nRjkdfn+6XnmrLeLS6dA/gTj03tJ3YN
JoqkjAcL2+x0SU3PtDYJO29TFOvIWlq2iJyTukYdlSXLhY5U3hyv/BdgI9gd6D2T
c6sy9i3CnkKSBlPniRQC2bor5ZzCLxr7NWMfe1HsAQExw6+iGwVtaNjP4wX2kMzA
w6cPNYKsZqpjXx8/GzkralkXZvBhW6IvVQe4EZjZW8MSoK7Gb6IAV+BM0ltOasY7
OOPQvTjL/3Aj0KJSAjrpbdFzYzwpIqUpwYFKW53y9eBnd2QlarrOnOGsdRBbCctV
2QIDAQAB
-----END PUBLIC KEY-----
`

const privateKey = `-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAzIXNCB3YktXiCiXiN1yRW+Ox9IqN2aenMKG9NHdOBlwp/2BQ
km+G4nRjkdfn+6XnmrLeLS6dA/gTj03tJ3YNJoqkjAcL2+x0SU3PtDYJO29TFOvI
Wlq2iJyTukYdlSXLhY5U3hyv/BdgI9gd6D2Tc6sy9i3CnkKSBlPniRQC2bor5ZzC
Lxr7NWMfe1HsAQExw6+iGwVtaNjP4wX2kMzAw6cPNYKsZqpjXx8/GzkralkXZvBh
W6IvVQe4EZjZW8MSoK7Gb6IAV+BM0ltOasY7OOPQvTjL/3Aj0KJSAjrpbdFzYzwp
IqUpwYFKW53y9eBnd2QlarrOnOGsdRBbCctV2QIDAQABAoIBACc4oZEk6BuAmNCJ
Y1BqmBWfHMlgqMNMu2tAGSCuoG/nzMYEmm76pEtZNp8JYJuJvViVZLYVclcIg/e/
YfNnWC5D+DpCP6v1NHe6TFKq6ipTtwMUFF//dXHNVScruxCXJuh92xidN8KIWQ+G
qnWXGWfdNPCw5dmjuo0sGgLXq5RFH4zKQJKVYB2t6E//PJsG5ultPUDl/+WgWOHe
+zq1U7r0bnQlqaZtzEhgJP0oQsuYKePWGcs8RpcKV3fTTY30HHIYKog5JL1FMV1I
nudG6q6Tug/3OpV80wGZujTKAiQCuW6lcov2IhKdwxPMEReCqqd6jo2m+GRF8lbE
EpvPc2ECgYEA952raix8lDkU/6iY4sVJP85N/pDIedT2Juy/VD3MpO3fZiASQqoV
5L7l9E9eb5iJEdL+3nhFQeV2VEScFpBoY+xhIbab0gT9qm8dhjm8dllpamsLcKrs
PoGXbhwu+NcIHUizXwOGBaO6/i1FuTJ/rNSjsyYHthXYY9rCha2YJ3sCgYEA03KZ
kMhSvKMMAY50VsDHtk+StvgqW8tI3LxuIL3i+MtY/IERSwSd5AUqMPEQWKpRS5EC
q2+0jy5eFwFm5aQManac6FctWgcSkgq3lgEC4pTguLNFbASuFzRie1ALJZbZrwxh
VJp2r+pI3s61VeGcU3FFSye+itrUBqxsmolLzbsCgYBM3mSNZFwUQ5gyOaukkmxH
44qw4U9rCuKTeOF4jGrQNIwqjwA8M8LyLRUD//OoHylGIENA2wNdDpfqVxZBpvjR
NFt+9Mpwq134H+CBf8Dy2JTyFWMKyfTm/qH868DlPRPmy1/ruhNMAuUU7Qb9FCEw
jR54ifDQ5P01Gn9Ssm5OqwKBgHkD6afXPqL/net2IFdWVfadbBaTyYpnufe7UDwk
8TX7C57YL5GDvum1mwQPs49LSuO4xpJfiDM6EleQUde0H/b+k6bV3frceWBkCdYs
Ff6fvk13LJA5zXkyXfq9QOPuhf+NUlcdYDgmGjaKj3XrfZC0DziIMqE9xINdQ3re
gSfpAoGAFvrknxQiS+c9IUGhjPbjJlLWkJwPKRyU20kTpvQGgmncrbLvY5vCeFla
b2zIosCQoyjL2ld2JHUHk/JUrSOXAHWbRFIJv5kExrJUws//wXtWDFtVnz9fs1Im
DKt9oGANzzAbRMfur9rydmujGR/TNkbkAWXI4g/toIiLlxlDQX8=
-----END RSA PRIVATE KEY-----
`

type fixtureRoundTripper struct {
	response *http.Response
	e        error
	request  *http.Request
}

func (r *fixtureRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	r.request = req
	return r.response, r.e
}

type fixtureFetcher struct {
	value interface{}
	e     error
}

func (f *fixtureFetcher) Fetch(string) (interface{}, error) {
	return f.value, f.e
}

func TestPrivateKeyParser(t *testing.T) {
	var _, e = NewPrivateKey([]byte(privateKey))
	if e != nil {
		t.Fatalf("Could not parse private key %s", e)
	}
}

func TestPrivateKeyParserEncoded(t *testing.T) {
	var data = dataurl.EncodeBytes([]byte(privateKey))
	var _, e = NewPrivateKey([]byte(data))
	if e != nil {
		t.Fatalf("Could not parse private key %s", e)
	}
}

func TestMicrosPrivateKeyParserEncoded(t *testing.T) {
	var data = dataurl.EncodeBytes([]byte(privateKey))
	os.Setenv("ASAP_PRIVATE_KEY", data)
	defer os.Unsetenv("ASAP_PRIVATE_KEY")
	var _, e = NewMicrosPrivateKey()
	if e != nil {
		t.Fatalf("Could not parse private key %s", e)
	}
}
func TestPublicKeyParser(t *testing.T) {
	var _, e = NewPublicKey([]byte(publicKey))
	if e != nil {
		t.Fatalf("Could not parse public key %s", e)
	}
}

func TestHTTPFetcherJoinsKidToPath(t *testing.T) {
	var response = &http.Response{
		StatusCode: http.StatusInternalServerError,
		Body:       ioutil.NopCloser(bytes.NewBufferString(``)),
	}
	var transport = &fixtureRoundTripper{response, nil, nil}
	var client = &http.Client{Transport: transport}

	var f = NewHTTPKeyFetcher("http://localhost", client)
	f.Fetch("TEST")
	if transport.request.URL.String() != "http://localhost/TEST" {
		t.Fatalf("HTTP fetcher did not use the key id in the path.")
	}
}

func TestHTTPFetcher(t *testing.T) {
	var response = &http.Response{
		StatusCode: http.StatusOK,
		Body:       ioutil.NopCloser(bytes.NewBufferString(publicKey)),
	}
	var transport = &fixtureRoundTripper{response, nil, nil}
	var client = &http.Client{Transport: transport}

	var f = NewHTTPKeyFetcher("http://localhost", client)
	var _, e = f.Fetch("TEST")
	if e != nil {
		t.Fatalf("HTTP fetcher did not parse the response body.")
	}
}

func TestCacheFetcher(t *testing.T) {
	var value = "TEST"
	var kid = "keyID"
	var wrapped = &fixtureFetcher{value, nil}
	var f = NewCachingFetcher(wrapped).(*cacheFetcher)
	var k, _ = f.Fetch(kid)
	if k != value {
		t.Fatalf("Expected to get TEST but instead got %s", k)
	}
	if f.cache[kid] != value {
		t.Fatalf("Expected to find a cache entry but found %s", f.cache[kid])
	}

	wrapped.value = "TEST2"
	k, _ = f.Fetch(kid)
	if k != value {
		t.Fatalf("Expected to get the cached value but instead got %s", k)
	}
}

func TestMultiFetcherSuccess(t *testing.T) {
	var failure = &http.Response{
		StatusCode: http.StatusInternalServerError,
		Body:       ioutil.NopCloser(bytes.NewBufferString(``)),
	}
	var success = &http.Response{
		StatusCode: http.StatusOK,
		Body:       ioutil.NopCloser(bytes.NewBufferString(publicKey)),
	}
	var successClient = &http.Client{Transport: &fixtureRoundTripper{success, nil, nil}}
	var failureClient = &http.Client{Transport: &fixtureRoundTripper{failure, nil, nil}}

	var f = NewMultiFetcher(
		NewHTTPKeyFetcher("http://localhost", failureClient),
		NewHTTPKeyFetcher("http://localhost", successClient),
	)
	_, e := f.Fetch("TEST")
	if e != nil {
		t.Fatalf("MultiFetcher fetcher did not parse the response body.")
	}
}

func TestMultiFetcherFailure(t *testing.T) {
	var failure = &http.Response{
		StatusCode: http.StatusInternalServerError,
		Body:       ioutil.NopCloser(bytes.NewBufferString(``)),
	}
	var failureClient = &http.Client{Transport: &fixtureRoundTripper{failure, nil, nil}}

	var f = NewMultiFetcher(
		NewHTTPKeyFetcher("http://localhost", failureClient),
		NewHTTPKeyFetcher("http://localhost", failureClient),
	)
	_, e := f.Fetch("TEST")
	if e == nil {
		t.Fatalf("MultiFetcher fetcher did fail when all delegate fetchers failed.")
	}
}

func TestExpiringHTTPFetcher(t *testing.T) {
	var expirationTime = time.Now().AddDate(0, 0, 2)
	var response = &http.Response{
		StatusCode: http.StatusOK,
		Header:     map[string][]string{"Expires": {expirationTime.UTC().Format(http.TimeFormat)}},
		Body:       ioutil.NopCloser(bytes.NewBufferString(publicKey)),
	}
	var transport = &fixtureRoundTripper{response, nil, nil}
	var client = &http.Client{Transport: transport}

	var fetcher, e = NewExpiringCacheFetcher("http://localhost", client, 3000)
	if e != nil {
		t.Fatalf("Expiring Cache fetcher constructor did not succeed.")
	}

	var f = fetcher.(*expiringCacheFetcher)
	_, e = f.Fetch("TEST")
	if e != nil {
		t.Fatalf("Expiring Cache fetcher did not parse the response body.")
	}
	value, ok := f.cache.Load("TEST")
	if !ok {
		t.Fatalf("Expiring Cache fetcher does not contain a entry under TEST")
	}
	expiringKeyPair, _ := value.(keyExpirationPair)
	if expiringKeyPair.expiration.UTC().Format(http.TimeFormat) != expirationTime.UTC().Format(http.TimeFormat) {
		t.Fatalf("Expiring Cache Fetcher cached %s as expiry date, but expecting %s",
			expiringKeyPair.expiration.UTC().Format(http.TimeFormat), expirationTime.UTC().Format(http.TimeFormat))
	}

	f.cache.Store("NEWKEY", keyExpirationPair{"newkey", expirationTime})
	key, e := f.Fetch("NEWKEY")
	if e != nil {
		t.Fatalf("Expiring Cache fetcher did not parse the response body when item is cached")
	}
	value, ok = f.cache.Load("NEWKEY")
	if !ok {
		t.Fatalf("Expiring Cache fetcher did not contain key NEWKEY")
	}
	expiringKeyPair, _ = value.(keyExpirationPair)
	if key != expiringKeyPair.key {
		t.Fatalf("Expiring Cache fetcher did not use cached value")
	}

}

func TestGetExpiryDate(t *testing.T) {
	timeNow := func() time.Time {
		return time.Time{}.Add(time.Hour * 3)
	}
	r, _ := regexp.Compile(maxAgeRegex)

	var expiryTestTable = []struct {
		in  http.Header
		out time.Time
	}{
		{http.Header{"Cache-Control": {"public, max-age=1200"}},
			timeNow().Add(time.Second * 1200)},
		{http.Header{"Cache-Control": {"max-age=1200", "post-check=0", "pre-check=0"}},
			timeNow().Add(time.Second * 1200)},
		{http.Header{"Cache-Control": {"max-age=lol", "post-check=0", "pre-check=0"}},
			time.Time{}},
		{http.Header{
			"Cache-Control": {"max-age=lol", "post-check=0", "pre-check=0"},
			"Expires":       []string{timeNow().Add(time.Second * 10).Format(http.TimeFormat)}},
			timeNow().Add(time.Second * 10)},
		{http.Header{"Expires": {timeNow().Add(time.Second * 10).Format(http.TimeFormat)}},
			timeNow().Add(time.Second * 10)},
		{http.Header{"Expires": {"lol"}}, time.Time{}},
		{http.Header{}, time.Time{}},
	}

	for _, tt := range expiryTestTable {
		expiryTime := getExpiryTime(tt.in, r, timeNow)
		if expiryTime != tt.out {
			t.Fatalf("getExpiryDate(%s) returned %s instead of the expected %s",
				tt.in, expiryTime.String(), tt.out.String())
		}
	}
}

func TestExpiringHTTPFetcherCache(t *testing.T) {
	timeNow := func() time.Time {
		return time.Time{}.Add(time.Hour * 3)
	}
	var response = &http.Response{
		StatusCode: http.StatusOK,
		Header:     map[string][]string{"Cache-Control": {"max-age=1200"}},
		Body:       ioutil.NopCloser(bytes.NewBufferString(publicKey)),
	}
	var transport = &fixtureRoundTripper{response, nil, nil}
	var client = &http.Client{Transport: transport}

	fetcher, e := NewExpiringCacheFetcher("http://localhost", client, 3000)
	if e != nil {
		t.Fatalf("Expiring Cache fetcher constructor did not succeed.")
	}

	f := fetcher.(*expiringCacheFetcher)
	f.timeNow = timeNow
	_, e = f.Fetch("TEST")
	if e != nil {
		t.Fatalf("Expiring Cache fetcher did not parse the response body.")
	}

	value, ok := f.cache.Load("TEST")
	if !ok {
		t.Fatalf("Expiring Cache fetcher does not contain a entry under TEST")
	}
	expiringKeyPair, _ := value.(keyExpirationPair)
	if expiringKeyPair.expiration != timeNow().Add(1200*time.Second) {
		t.Fatalf("Expiring Cache Fetcher does not return correct expiry time from Cache-Control header")
	}
}

type lockingFixtureRoundTripper struct {
	response *http.Response
	e        error
	request  *http.Request
	lock     *sync.Mutex
}

func (r *lockingFixtureRoundTripper) RoundTrip(req *http.Request) (*http.Response, error) {
	r.lock.Lock()
	defer r.lock.Unlock()
	r.request = req
	return r.response, r.e
}

func (r *lockingFixtureRoundTripper) SetResponse(res *http.Response, err error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	r.response = res
	r.e = err
}

func (r *lockingFixtureRoundTripper) GetRequest() *http.Request {
	r.lock.Lock()
	defer r.lock.Unlock()
	return r.request
}

func TestExpiringHTTPFetcherCacheRefresh(t *testing.T) {
	var response = &http.Response{
		StatusCode: http.StatusOK,
		Header:     map[string][]string{"Expires": {time.Now().UTC().Add(time.Second).Format(http.TimeFormat)}},
		Body:       ioutil.NopCloser(bytes.NewBufferString(publicKey)),
	}
	var transport = &lockingFixtureRoundTripper{response, nil, nil, &sync.Mutex{}}
	var client = &http.Client{Transport: transport}
	fetcher, e := NewExpiringCacheFetcher("http://localhost", client, time.Second)
	if e != nil {
		t.Fatalf("Expiring Cache fetcher constructor did not succeed.")
	}
	f := fetcher.(*expiringCacheFetcher)
	_, err := f.Fetch("KEY")
	if err != nil {
		t.Fatalf("Fetch returned error: " + err.Error())
	}

	// Test initial re-fetch
	newExpiryTime := time.Now().UTC().Add(time.Second).Format(http.TimeFormat)
	response = &http.Response{
		StatusCode: http.StatusOK,
		Header:     map[string][]string{"Expires": {newExpiryTime}},
		Body:       ioutil.NopCloser(bytes.NewBufferString(publicKey)),
	}
	transport.SetResponse(response, nil)

	time.Sleep(time.Millisecond)

	value, ok := f.cache.Load("KEY")
	if !ok {
		t.Fatalf("Cache did not contain key KEY")
	}
	pair, _ := value.(keyExpirationPair)
	if pair.expiration.Format(http.TimeFormat) != newExpiryTime {
		t.Fatalf("Cache refresh goroutine did not run correctly")
	}

	// Test second spawned re-fetch goroutine
	newExpiryTime = time.Now().UTC().Add(time.Second).Format(http.TimeFormat)
	response = &http.Response{
		StatusCode: http.StatusOK,
		Header:     map[string][]string{"Expires": {newExpiryTime}},
		Body:       ioutil.NopCloser(bytes.NewBufferString(publicKey)),
	}
	transport.SetResponse(response, nil)

	time.Sleep(time.Millisecond)

	value, _ = f.cache.Load("KEY")
	pair, _ = value.(keyExpirationPair)
	if pair.expiration.Format(http.TimeFormat) != newExpiryTime {
		t.Fatalf("Cache refresh goroutine did not run correctly")
	}
	if pair.expiration.Format(http.TimeFormat) != newExpiryTime {
		t.Fatalf("Cache refresh goroutine did not run correctly")
	}
}
